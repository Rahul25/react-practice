export const FETCH_POSTS = 'FETCH_POSTS';
export const NEW_POST = 'NEW_POST';
export const DELETE_POST = 'DELETE_POST';

// User Action Types
export const FETCH_USERS = 'FETCH_USERS';