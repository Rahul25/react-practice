import { FETCH_POSTS, NEW_POST, DELETE_POST } from './types';

export const fetchPosts = () => dispatch => {
    
    fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(posts => dispatch({
            type: FETCH_POSTS,
            payload: posts
        }))       
}

export const createPost = (postData) => dispatch => {

    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'post',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(postData)
    }).then(response => response.json())
        .then(data => dispatch({
            type: NEW_POST,
            payload: data
        }))
}

export const deletePost = () => dispatch => {
    console.log('delete post action dispatching');
    dispatch({
        type: DELETE_POST
    })
}