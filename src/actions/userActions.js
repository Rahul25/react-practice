import { FETCH_USERS } from './types';

export const fetchUsers = () => dispatch => {

    fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(users => dispatch({
            type: FETCH_USERS,
            payload: users
        }))

}