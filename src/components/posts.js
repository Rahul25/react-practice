import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions/postActions';

class Posts extends React.Component {

    componentWillMount = () => {
        this.props.fetchPosts();
    }

    render() {

        const {posts} = this.props;
        const postItems = posts.map( post => (
            <div key={post.id}>
                <h3>{post.title}</h3>
                <p>{post.body}</p>
            </div>
        ));

        return (
            <div className="post-wrapper">{postItems}</div>
        );
    }
}

/* Posts.propTypes = {
    fetchPosts: PropTypes.func.isRequired,
    posts: PropTypes.array.isRequired,
    newPost: PropTypes.object
} */

const mapStateToProps = state => {
    return {
        posts: state.posts.items,
        newPost: state.posts.item
    }
}

export default connect(mapStateToProps, {fetchPosts})(Posts);
