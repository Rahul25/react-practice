import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createPost } from '../actions/postActions';

class PostForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            body: ''
        };
    }

    changeFormValue = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log('form submitted');

        let post = {
            title: this.state.title,
            body: this.state.body
        }

        this.props.createPost(post);
    }

    render() {
        return (
            <>
            <h1>Add Post</h1>
            <form>
                <div>
                    <label>Post Title: </label><br />
                    <input type="text" name="title" onChange={e=>this.changeFormValue(e)} value={this.state.title} />
                </div>
                <div>
                    <label>Post Body: </label><br />
                    <input type="text" name="body" onChange={e => this.changeFormValue(e)} value={this.state.body} />
                </div>
                <input type="submit" value="save" onClick={e=>{this.handleSubmit(e)}} />
            </form>
            </>
        );
    }
}

PostForm.propTypes = {
    createPost: PropTypes.func.isRequired
}

export default connect(null, {createPost})(PostForm);
