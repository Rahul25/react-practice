import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchUsers } from '../actions/userActions';
import { deletePost } from '../actions/postActions';

class User extends React.Component {

    componentDidMount = () => {
        this.props.fetchUsers();
    }

    render() {

        const {users} = this.props;
        const userList = users.map(user => (
            <li key={user.id}>{user.name}</li>
        ));

        return (
            <>
            <h1>User List</h1>
            <ul>{userList}</ul>
                <button onClick={this.props.deletePost}>Remove Post</button>
            </>
        );
    }
}

/* User.propTypes = {
    fetchUsers: PropTypes.func.isRequired,
    users: PropTypes.array.isRequired
}; */

const mapStateToProps = state => {
    return {
        users: state.users.items,
    }
}

export default connect(mapStateToProps, { fetchUsers, deletePost })(User);
