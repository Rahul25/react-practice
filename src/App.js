import React from 'react';
import Posts from "./components/posts";
import PostForm from "./components/postForm";
import User from "./components/user";
import { Provider } from "react-redux";

import Store from './store';





function App() {
  return (
    <Provider store={Store}>
      <div className="App">
        <User/>
      </div>
      <hr />
      <div className="App">
        <PostForm />
        <hr />
        <Posts />
      </div>
    </Provider>
  );
}

export default App;
