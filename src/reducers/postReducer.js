import { FETCH_POSTS, NEW_POST, DELETE_POST } from '../actions/types';

const initialState = {
    items: []
}

export default function( state=initialState, action ){

    switch(action.type){

        case FETCH_POSTS:
            return {
                ...state,
                items: action.payload
            }

        case NEW_POST:
            let posts = [...state.items, action.payload];
            posts.unshift(action.payload);
            return {
                ...state,
                items: posts
            }

        case DELETE_POST:
            let postsAfterDelete = [...state.items];
            postsAfterDelete.shift();
            return {
                ...state,
                items: postsAfterDelete
            };
            
        default:
            return state;
    }
}